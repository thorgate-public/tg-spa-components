"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _gatsbyImage = _interopRequireDefault(require("gatsby-image"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./AboutCompany.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var AboutCompany = function AboutCompany(_ref) {
  var caseStudies = _ref.caseStudies;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "about-company"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column align-items-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "about-company-title text-center"
  }, "Thorgate: The product company"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "about-company-paragraph col-lg-8 col-md-10 col-sm-12 text-center"
  }, /*#__PURE__*/_react["default"].createElement("p", null, "Thorgate is a group of IT and growth companies aiming to change the world with technology through access to the ecosystem, digital products, and venture funding.", ' '), /*#__PURE__*/_react["default"].createElement("p", null, "Our product development team supports our fields of science and innovation, and also the companies we invest in. We deliver quality digital products built with Python, Django, React.js and React Native."), /*#__PURE__*/_react["default"].createElement("p", null, "We also invest our know-how and capital in innovative startups and help them succeed. Thorgate Ventures is a boutique incubator with hand-picked projects and people that we really want to work with."))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-section-title text-center"
  }, "Case studies"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "about-company-sub-header text-center"
  }, "Some of our digitally transforming projects"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "container d-flex flex-wrap about-company-case-studies"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column col-lg-4 col-md-4 col-sm-10"
  }, /*#__PURE__*/_react["default"].createElement(_gatsbyImage["default"], {
    fluid: caseStudies.ecoop.childImageSharp.fluid
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "case-study-title text-center"
  }, "eCoop"), /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://thorgate.eu/ecoop/",
    target: "_blank",
    rel: "noopener noreferrer",
    className: "text-center"
  }, "View case study")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column col-lg-4 col-md-4 col-sm-10"
  }, /*#__PURE__*/_react["default"].createElement(_gatsbyImage["default"], {
    fluid: caseStudies.novastar.childImageSharp.fluid
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "case-study-title text-center"
  }, "Novastar"), /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://thorgate.eu/novastar/",
    target: "_blank",
    rel: "noopener noreferrer",
    className: "text-center"
  }, "View case study")), /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column col-lg-4 col-md-4  col-sm-10"
  }, /*#__PURE__*/_react["default"].createElement(_gatsbyImage["default"], {
    fluid: caseStudies.krah.childImageSharp.fluid
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "case-study-title text-center"
  }, "Krah Pipes"), /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://thorgate.eu/krah-pipes/",
    target: "_blank",
    rel: "noopener noreferrer",
    className: "text-center"
  }, "View case study")))));
};

AboutCompany.propTypes = {
  caseStudies: _propTypes["default"].shape({
    ecoop: _propTypes["default"].object.isRequired,
    novastar: _propTypes["default"].object.isRequired,
    krah: _propTypes["default"].object.isRequired
  }).isRequired
};
var _default = AboutCompany;
exports["default"] = _default;