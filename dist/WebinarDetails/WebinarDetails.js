"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./WebinarDetails.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var WebinarDetails = function WebinarDetails(_ref) {
  var speakerInfo = _ref.speakerInfo,
      attendeeInfo = _ref.attendeeInfo,
      renderImage = _ref.renderImage;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-details d-flex justify-content-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-wrap col-lg-9 col-md-10 col-sm-12"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-details-paragraph d-flex flex-column col-lg-6 col-md-6 col-sm-12"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "speaker-profile-pic"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    style: {
      maxWidth: 200
    }
  }, renderImage(speakerInfo.image))), /*#__PURE__*/_react["default"].createElement("div", {
    className: "section-subtitle"
  }, speakerInfo.title), /*#__PURE__*/_react["default"].createElement("a", {
    href: speakerInfo.speakerLinkedinUrl,
    target: "_blank",
    rel: "noopener noreferrer"
  }, speakerInfo.speakerName), /*#__PURE__*/_react["default"].createElement(_reactMarkdown["default"], {
    source: speakerInfo.contents,
    escapeHtml: false
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-details-paragraph d-flex flex-column col-lg-6 col-md-6 col-sm-12"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-details-sub-title"
  }, attendeeInfo.subTitleOne), /*#__PURE__*/_react["default"].createElement(_reactMarkdown["default"], {
    source: attendeeInfo.subTitleOneContents,
    escapeHtml: false
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-details-sub-title"
  }, attendeeInfo.subTitleTwo), /*#__PURE__*/_react["default"].createElement(_reactMarkdown["default"], {
    source: attendeeInfo.subTitleTwoContents,
    escapeHtml: false
  }))));
};

WebinarDetails.propTypes = {
  speakerInfo: _propTypes["default"].object.isRequired,
  attendeeInfo: _propTypes["default"].object.isRequired,
  renderImage: _propTypes["default"].func.isRequired
};
var _default = WebinarDetails;
exports["default"] = _default;