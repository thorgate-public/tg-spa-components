"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "WebinarDetails", {
  enumerable: true,
  get: function get() {
    return _WebinarDetails["default"];
  }
});

var _WebinarDetails = _interopRequireDefault(require("./WebinarDetails"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }