"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "NetlifySubmissionForm", {
  enumerable: true,
  get: function get() {
    return _NetlifySubmissionForm["default"];
  }
});

var _NetlifySubmissionForm = _interopRequireDefault(require("./NetlifySubmissionForm"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }