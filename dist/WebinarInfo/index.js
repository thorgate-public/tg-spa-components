"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "WebinarInfo", {
  enumerable: true,
  get: function get() {
    return _WebinarInfo["default"];
  }
});

var _WebinarInfo = _interopRequireDefault(require("./WebinarInfo"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }