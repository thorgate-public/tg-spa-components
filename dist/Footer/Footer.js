"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

require("./Footer.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ThorgateLogo = function ThorgateLogo(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M140.294 281.99C62.811 281.99 0 218.86 0 140.99S62.811 0 140.294 0s140.293 63.13 140.293 140.99c0 77.86-62.811 141-140.293 141zm-.044-53.93l49.61-22.16v-70.02L172 132.4l-.015 61.63-31.738 14.29-31.377-14.3.007-61.65-17.871 3.49v70.15l49.244 22.05zm49.61-158.39l-49.579-8.65-49.272 8.65v18.27l49.272-8.73 49.579 8.73V69.67zm9.2 29.74l-58.783-11.44-58.419 11.41v28.91l36.168-7.09v66.99l22.251 9.96 22.55-9.96v-66.94l36.233 7.08V99.41z"
  }));
};

ThorgateLogo.defaultProps = {
  viewBox: "0 0 281 282"
};

var FacebookIcon = function FacebookIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("title", null, "Path"), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M4.726 16.814V8.406h2.102l.28-2.897H4.725l.003-1.45c0-.756.066-1.161 1.049-1.161h1.314V0H4.99C2.463 0 1.574 1.406 1.574 3.77v1.739H0v2.897h1.574v8.408h3.152z",
    fill: "#8B97A6",
    fillRule: "evenodd"
  }));
};

FacebookIcon.defaultProps = {
  width: "8",
  height: "17",
  viewBox: "0 0 8 17",
  xmlns: "http://www.w3.org/2000/svg"
};

var TwitterIcon = function TwitterIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("title", null, "Path"), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M7.283 3.192l.033.52-.542-.064c-1.973-.244-3.696-1.07-5.16-2.458L.9.502l-.185.508c-.39 1.134-.14 2.33.672 3.136.434.444.336.508-.412.243-.26-.085-.487-.148-.509-.116-.076.074.184 1.038.39 1.419.282.53.856 1.049 1.485 1.356l.531.243-.628.011c-.607 0-.63.01-.564.233.217.689 1.073 1.42 2.027 1.737l.672.223-.586.339a6.255 6.255 0 0 1-2.904.784c-.488.01-.889.053-.889.084 0 .106 1.322.7 2.092.932 2.308.689 5.05.392 7.11-.783 1.463-.837 2.926-2.5 3.609-4.11.368-.858.737-2.426.737-3.178 0-.487.032-.55.64-1.133.357-.34.693-.71.758-.816.108-.201.098-.201-.455-.021-.921.317-1.051.275-.596-.202.336-.339.737-.953.737-1.133 0-.032-.163.021-.347.117-.195.106-.629.264-.954.36l-.585.18-.531-.35c-.293-.19-.705-.402-.921-.466-.553-.148-1.399-.127-1.897.043-1.355.476-2.211 1.705-2.114 3.05z",
    fill: "#8B97A6",
    fillRule: "evenodd"
  }));
};

TwitterIcon.defaultProps = {
  width: "15",
  height: "12",
  viewBox: "0 0 15 12",
  xmlns: "http://www.w3.org/2000/svg"
};

var LinkedInIcon = function LinkedInIcon(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("title", null, "Group"), /*#__PURE__*/_react["default"].createElement("g", {
    fill: "#8B97A6",
    fillRule: "evenodd"
  }, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M3.71 4.924H.206v10.213H3.71zM3.94 1.764C3.917.763 3.18 0 1.981 0 .783 0 0 .763 0 1.764c0 .98.76 1.766 1.936 1.766h.022c1.222 0 1.982-.785 1.982-1.766zM16.34 9.281c0-3.137-1.728-4.597-4.032-4.597-1.86 0-2.692.991-3.157 1.687V4.924H5.648c.046.958 0 10.213 0 10.213h3.503V9.433c0-.305.023-.61.115-.828.253-.61.83-1.241 1.797-1.241 1.268 0 1.775.936 1.775 2.309v5.464h3.502V9.281z"
  })));
};

LinkedInIcon.defaultProps = {
  width: "17",
  height: "16",
  viewBox: "0 0 17 16",
  xmlns: "http://www.w3.org/2000/svg"
};

var Footer = function Footer() {
  return /*#__PURE__*/_react["default"].createElement("footer", {
    className: "footer"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "container footer__container"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "footer__left"
  }, /*#__PURE__*/_react["default"].createElement(ThorgateLogo, {
    className: "footer__logo"
  }), "\xA9", ' ', new Date().getFullYear(), " Thorgate"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "footer__right"
  }, /*#__PURE__*/_react["default"].createElement("ul", {
    className: "footer__links"
  }, /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://thorgate.eu/"
  }, "Thorgate")), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://design.thorgate.eu/"
  }, "Design")), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "http://thorgateventures.com/"
  }, "Ventures")), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "http://thorgate.github.io/"
  }, "Labs")), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://jobs.thorgate.eu/"
  }, "Jobs")), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://pythonestonia.ee/"
  }, "PythonEstonia"))), /*#__PURE__*/_react["default"].createElement("ul", {
    className: "footer__social-links"
  }, /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__social-link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://www.facebook.com/thorgate"
  }, /*#__PURE__*/_react["default"].createElement(FacebookIcon, null))), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__social-link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://twitter.com/thorgate"
  }, /*#__PURE__*/_react["default"].createElement(TwitterIcon, null))), /*#__PURE__*/_react["default"].createElement("li", {
    className: "footer__social-link"
  }, /*#__PURE__*/_react["default"].createElement("a", {
    href: "https://www.linkedin.com/company/thorgate"
  }, /*#__PURE__*/_react["default"].createElement(LinkedInIcon, {
    className: "footer__icon--linkedin"
  })))))));
};

var _default = Footer;
exports["default"] = _default;