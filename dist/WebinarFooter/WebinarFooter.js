"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = require("../Button");

require("./WebinarFooter.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var WebinarFooter = function WebinarFooter() {
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-footer d-flex flex-column align-items-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-footer-title text-center"
  }, "Don't miss this webinar"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "col-lg-8 col-md-10 col-sm-12 text-center"
  }, /*#__PURE__*/_react["default"].createElement("p", null, "Attend this free webinar to learn more about the best practices of product development in the current crisis to help you save time, money and resources!")), /*#__PURE__*/_react["default"].createElement(_Button.LinkButton, {
    href: "#form"
  }, "Save my spot"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-footer-email text-center"
  }, "Can't attend? Email us at", ' ', /*#__PURE__*/_react["default"].createElement("a", {
    href: "mailto:hi@thorgate.eu"
  }, "hi@thorgate.eu"), " for an personal consultation instead!"));
};

var _default = WebinarFooter;
exports["default"] = _default;