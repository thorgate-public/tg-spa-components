import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Formik} from 'formik';
import queryString from 'querystring';

import './NetlifySubmissionForm.scss';

const NetlifySubmissionForm = ({
    initialValues,
    title,
    onSubmitText,
    validationSchema,
    children,
}) => {
    const [submitted, setSubmitted] = useState(false);
    if (submitted) {
        return <p>{onSubmitText}</p>;
    }

    return (
        <div id="form">
            <div className="form-title text-center">{title}</div>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                    fetch('/', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: queryString.stringify({
                            'form-name': 'submission',
                            ...values,
                        }),
                    })
                        .then(() => setSubmitted(true))
                        .catch((error) => console.error(error));
                }}
            >
                {(props) => children(props)}
            </Formik>
            <div className="privacy-link text-center">
                <a href="https://thorgate.eu/privacy/">
                    We respect your privacy.
                </a>
            </div>
        </div>
    );
};

NetlifySubmissionForm.propTypes = {
    initialValues: PropTypes.shape({}).isRequired,
    title: PropTypes.shape({}).isRequired,
    onSubmitText: PropTypes.string.isRequired,
    validationSchema: PropTypes.shape({}).isRequired,
    children: PropTypes.func.isRequired,
};

export default NetlifySubmissionForm;
