import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const LinkButton = ({children, ...rest}) => (
    <a className="tg-button" {...rest}>
        {children}
    </a>
);

LinkButton.propTypes = {
    children: PropTypes.node.isRequired,
};

export default LinkButton;
