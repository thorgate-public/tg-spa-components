import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';

import './WebinarDetails.scss';

const WebinarDetails = ({speakerInfo, attendeeInfo, renderImage}) => (
    <div className="webinar-details d-flex justify-content-center">
        <div className="d-flex flex-wrap col-lg-9 col-md-10 col-sm-12">
            <div className="webinar-details-paragraph d-flex flex-column col-lg-6 col-md-6 col-sm-12">
                <div className="speaker-profile-pic">
                    <div style={{maxWidth: 200}}>
                        {renderImage(speakerInfo.image)}
                    </div>
                </div>

                <div className="section-subtitle">{speakerInfo.title}</div>
                <a
                    href={speakerInfo.speakerLinkedinUrl}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    {speakerInfo.speakerName}
                </a>
                <ReactMarkdown
                    source={speakerInfo.contents}
                    escapeHtml={false}
                />
            </div>
            <div className="webinar-details-paragraph d-flex flex-column col-lg-6 col-md-6 col-sm-12">
                <div className="webinar-details-sub-title">
                    {attendeeInfo.subTitleOne}
                </div>
                <ReactMarkdown
                    source={attendeeInfo.subTitleOneContents}
                    escapeHtml={false}
                />
                <div className="webinar-details-sub-title">
                    {attendeeInfo.subTitleTwo}
                </div>
                <ReactMarkdown
                    source={attendeeInfo.subTitleTwoContents}
                    escapeHtml={false}
                />
            </div>
        </div>
    </div>
);

WebinarDetails.propTypes = {
    speakerInfo: PropTypes.object.isRequired,
    attendeeInfo: PropTypes.object.isRequired,
    renderImage: PropTypes.func.isRequired,
};

export default WebinarDetails;
