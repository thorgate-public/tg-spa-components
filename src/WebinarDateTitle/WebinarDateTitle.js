import React from 'react';
import PropTypes from 'prop-types';

import {LinkButton} from '../Button';
import ThorgateLogo from './images/thorgate-logo.svg';
import './WebinarDateTitle.scss';

const weekday = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
];
const month = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
];

const nth = (d) => {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
        case 1:
            return 'st';
        case 2:
            return 'nd';
        case 3:
            return 'rd';
        default:
            return 'th';
    }
};

const withZero = (time) => (time < 10 ? '0' + time : time);
const formatTime = (date, duration = 0) =>
    `${withZero(date.getHours() + duration)}:${withZero(date.getMinutes())}`;

const WebinarDateTitle = ({date, title}) => (
    <div className="container__webinar-date-title d-flex flex-column align-items-center">
        <div className="logo-above-title">
            <ThorgateLogo />
        </div>
        <div className="webinar-price text-center">Free Webinar</div>
        <div className="webinar-title text-center pl-2 pr-2">{title}</div>
        <LinkButton href="#form">Save my spot</LinkButton>
        <div className="d-flex">
            <div className="d-flex flex-column align-items-center p-5">
                <span>Date:</span>
                <span className="text-muted text-center">
                    {weekday[date.getDay()]}, {date.getDate()}
                    {nth(date.getDate())} {month[date.getMonth()]},{' '}
                    {date.getFullYear()}
                </span>
            </div>
            <div className="line mt-5" />
            <div className="d-flex flex-column align-items-center p-5">
                <span>Time:</span>
                <span className="text-muted text-center">
                    {formatTime(date)} - {formatTime(date, 1)} EST
                </span>
            </div>
        </div>
    </div>
);

WebinarDateTitle.propTypes = {
    date: PropTypes.object.isRequired,
    title: PropTypes.node.isRequired,
};

export default WebinarDateTitle;
