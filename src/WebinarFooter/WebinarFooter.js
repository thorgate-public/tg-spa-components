import React from 'react';
import {LinkButton} from '../Button';

import './WebinarFooter.scss';

const WebinarFooter = () => (
    <div className="webinar-footer d-flex flex-column align-items-center">
        <div className="webinar-footer-title text-center">
            Don&#39;t miss this webinar
        </div>
        <div className="col-lg-8 col-md-10 col-sm-12 text-center">
            <p>
                Attend this free webinar to learn more about the best practices
                of product development in the current crisis to help you save
                time, money and resources!
            </p>
        </div>

        <LinkButton href="#form">Save my spot</LinkButton>
        <div className="webinar-footer-email text-center">
            Can&#39;t attend? Email us at{' '}
            <a href="mailto:hi@thorgate.eu">hi@thorgate.eu</a> for an personal
            consultation instead!
        </div>
    </div>
);

export default WebinarFooter;
