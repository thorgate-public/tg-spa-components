import React from 'react';
import PropTypes from 'prop-types';
import Countdown from 'react-countdown';

import './Timer.scss';

const Completed = ({children}) => (
    <div className="container">
        <div className="counter-title">{children}</div>
    </div>
);

Completed.propTypes = {
    children: PropTypes.node.isRequired,
};

const TimeBlock = ({number, label}) => (
    <div className="counter-block">
        <div>
            <div className="counter-number">{number}</div>
            <div className="counter-label">{label}</div>
        </div>
    </div>
);

TimeBlock.propTypes = {
    number: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
};

const Separator = () => <div className="counter-separator">:</div>;

const Clock = ({days, hours, minutes, title}) => (
    <div className="container">
        <div className="counter-title">
            <h2>{title}</h2>
        </div>
        <div className="counter-clock">
            <TimeBlock number={days} label="Days" />
            <Separator />
            <TimeBlock number={hours} label="Hours" />
            <Separator />
            <TimeBlock number={minutes} label="Minutes" />
        </div>
    </div>
);

Clock.propTypes = {
    days: PropTypes.number.isRequired,
    hours: PropTypes.number.isRequired,
    minutes: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
};

const Timer = ({date, title, gag}) => {
    const renderer = ({days, hours, minutes, completed}) => {
        if (completed) {
            // Render a completed state
            return <Completed>{gag}</Completed>;
        } else {
            // Render a countdown
            return (
                <Clock
                    days={days}
                    hours={hours}
                    minutes={minutes}
                    title={title}
                />
            );
        }
    };

    return <Countdown date={date} renderer={renderer} />;
};

Timer.propTypes = {
    date: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    gag: PropTypes.node.isRequired,
};

export default Timer;
